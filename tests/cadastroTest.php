<?php


use PHPUnit\Framework\TestCase;

use Antonio\Yourcompany\cadastro;

final class cadastroTest extends TestCase
{
    public function testCadastro() {
        $joao = new cadastro('joão','funcao','senha');
        $this->assertEquals($joao->nome, "joão");
    }

    public function testHiddenPassword() {
        $joao = new cadastro('joão','funcao','Senha@123');
        $this->assertNotEquals($joao->senha, 'Senha@123');
    }
}
